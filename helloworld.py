''' 
@Program: helloworld
@Author: Donald Osgood
@Last Date: 2023-11-29 10:45:22
@Purpose:Donald Osgood
'''
from flask import Flask
app = Flask(__name__)
 
@app.route("/")
def hello():
    return "Hello World!"
 
if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')